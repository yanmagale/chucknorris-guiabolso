Guia Bolso Challenge

> This project is a small category list to identify `Chuck Norris` personality (if its possible?!) and show current information about category description

# Setup

- Unzip files
- Acess folder and Run `yarn install`
- After install depedencies, run `yarn start`

# Stack

- React
- Redux
- Jest
- Eslint/Prettier
- CSS Modules

# Contact

Send a message to [this](yanmagale@gmail.com) e-mail
