import React, { Component } from 'react';
import { setCategoriesList } from '../../actions';
import ChuckNorrisList from './ChuckNorrisList';
import { connect } from 'react-redux';
import './categories-list.css';

class ChuckNorrisApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: [],
      category: '',
    };
    this.updateCategories = this.updateCategories.bind(this);
  }

  componentDidMount() {
    this.getCategories();
  }

  render() {
    const { categories } = this.props;
    return (
      <div className="container">
        <div className="category-list">
          <h2>Chuck Norries Categories List</h2>
          <ChuckNorrisList categories={categories} />
        </div>
      </div>
    );
  }

  updateCategories(categoriesList) {
    this.setState({ categories: categoriesList });
    let { dispatch } = this.props;
    let action = setCategoriesList(this.state.categories);
    dispatch(action);
  }

  getCategories() {
    fetch('https://api.chucknorris.io/jokes/categories')
      .then(response => response.json())
      .then(data => this.updateCategories(data))
      .catch(error => new Error('erro at get categories', error));
  }
}

const mapStateToProps = store => ({
  categories: store.categoriesState.categories,
});

export default connect(mapStateToProps)(ChuckNorrisApp);
