import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './categories-list.css';

class ChuckNorrisList extends Component {
  render() {
    const { categories } = this.props;
    return categories.map((category, key) => (
      <div className="category-item" key={key}>
        <Link to={`/category/${category}`}>
          <p className="category-text"> {category} </p>
        </Link>
      </div>
    ));
  }
}

export default ChuckNorrisList;
