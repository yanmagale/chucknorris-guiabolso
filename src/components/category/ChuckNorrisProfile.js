import React, { Component } from 'react';
import './category-profile.css';

class ChuckNorrisProfile extends Component {
  render() {
    const { profile } = this.props;
    return (
      <div>
        <h2 className="title">Chuck Norries Person Data</h2>
        <div className="category-container">
          <div className="category-icon-container">
            <img alt="Category Icon" src={profile.icon_url} />
          </div>
          <div className="category-descrption">
            <div className="title-category">
              <p> {profile.category}</p>
            </div>
            <div className="description-category">
              <p> {profile.value} </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ChuckNorrisProfile;
