import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { setProfile } from '../../actions';
import './category-profile.css';
import ChuckNorrisProfile from './ChuckNorrisProfile';

class ChuckNorrisCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      personInformation: [],
      category: '',
    };
    this.updatePersonInformation = this.updatePersonInformation.bind(this);
  }

  componentDidMount() {
    if (!!this.props.match.params.category) {
      this.getPersonInformationByCategory(this.props.match.params.category);
    }
  }

  render() {
    const { profile } = this.props;
    return (
      <div className="container">
        <ChuckNorrisProfile profile={profile} />
        <div className="back-action">
          <Link to="/">
            <p>Voltar para a página inicial</p>
          </Link>
        </div>
      </div>
    );
  }

  getPersonInformationByCategory(category) {
    fetch(`https://api.chucknorris.io/jokes/random?category=${category}`)
      .then(response => response.json())
      .then(data => this.updatePersonInformation(data))
      .catch(error => console.log('erro at get data', error));
  }

  updatePersonInformation(dataPerson) {
    this.setState({ personInformation: dataPerson });
    let { dispatch } = this.props;
    let action = setProfile(this.state.personInformation);
    dispatch(action);
  }
}

const mapStateToProps = store => ({
  profile: store.profileState.profile,
});

export default connect(mapStateToProps)(ChuckNorrisCategory);
