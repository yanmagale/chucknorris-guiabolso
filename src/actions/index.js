import { SET_CATEGORIES_LIST } from './actionTypes';
import { SET_PROFILE } from './actionTypes';


export const setCategoriesList = value => ({
    type: SET_CATEGORIES_LIST,
    categories: value
});

export const setProfile = value => ({
  type: SET_PROFILE,
  profile: value
});