import { SET_PROFILE } from '../actions/actionTypes';


const initialState = {
  profile: {}
};

export const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_PROFILE:
      return {
        ...state,
        profile: Object.assign(action.profile)
      };
    default:
      return state;
  }
};