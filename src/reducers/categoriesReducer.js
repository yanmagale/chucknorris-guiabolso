import { SET_CATEGORIES_LIST } from '../actions/actionTypes';


const initialState = {
  categories: []
};

export const categoriesReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CATEGORIES_LIST:
      return {
        ...state,
        categories: action.categories
      };
    default:
      return state;
  }
};