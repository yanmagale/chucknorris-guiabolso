import { categoriesReducer } from './categoriesReducer';
import { profileReducer } from './profileReducer';
import { combineReducers } from 'redux';

export const Reducers = combineReducers({
  categoriesState: categoriesReducer,
  profileState: profileReducer
});