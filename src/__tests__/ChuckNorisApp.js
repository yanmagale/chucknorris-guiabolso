// Link.react.test.js
import React from 'react';
import ChuckNorrisApp from '../ChuckNorrisApp';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import { Store } from '../store';

test('Render Chuck Norris Category Component', () => {
  const component = renderer.create(
    <Provider store={Store}>
      <ChuckNorrisApp />
    </Provider>
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
