import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import ChuckNorrisApp from './components/app/ChuckNorrisApp';
import ChuckNorrisCategory from './components/category/ChuckNorrisCategory';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import { Store } from './store';

ReactDOM.render(
  <Provider store={Store}>
    <BrowserRouter>
      <Switch>
        <Route path="/" exact={true} component={ChuckNorrisApp} />
        <Route path="/category/:category" component={ChuckNorrisCategory} />
      </Switch>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);
serviceWorker.unregister();
